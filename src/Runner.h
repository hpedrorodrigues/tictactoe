#ifndef TICTACTOE_RUNNER_H
#define TICTACTOE_RUNNER_H

#include <iostream>
#include "TicTacToe.h"

using namespace std;

class Runner {

private:
    TicTacToe *ticTacToe;

    void clearConsole();

public:
    Runner();

    void run();

    virtual ~Runner();

};

#endif