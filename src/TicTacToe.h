#ifndef TICTACTOE_TICTACTOE_H
#define TICTACTOE_TICTACTOE_H

#include <cstdlib>
#include <iostream>

using namespace std;

class TicTacToe {

private:
    static int const EMPTY = -1;
    static int const O = 0, X = 1;

    int board[9], userMoves[9], pcMoves[9];

    string displayValue(int position);

    bool userMove(int position);

    bool pcMove(int position);

    bool won(int moves[9]);

    void pcMove();

    bool defend(int a, int b, int c);

    bool isPlayedByUser(int position);

    bool isPlayedByPC(int position);

    bool isPlayedByBoth(int position);

public:
    TicTacToe();

    void userMove();

    bool userWon();

    bool pcWon();

    bool wasTied();

    void print();
};

#endif