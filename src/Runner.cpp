#include "Runner.h"

Runner::Runner() {
    this->ticTacToe = new TicTacToe();
}

Runner::~Runner() {
    delete this->ticTacToe;
}

void Runner::run() {
    while (!ticTacToe->userWon() && !ticTacToe->pcWon() && !ticTacToe->wasTied()) {
        clearConsole();
        ticTacToe->print();
        ticTacToe->userMove();
    }

    if (ticTacToe->userWon()) {
        cout << endl << "...You won..." << endl;
    } else if (ticTacToe->pcWon()) {
        cout << endl << "...You lose..." << endl;
    } else {
        cout << endl << "...Was tied..." << endl;
    }
}

void Runner::clearConsole() {
    system("clear");
}