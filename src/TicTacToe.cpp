#include "TicTacToe.h"

TicTacToe::TicTacToe() {
    for (register int i = 0; i < 9; i++) {
        this->board[i] = EMPTY;
        this->userMoves[i] = EMPTY;
        this->pcMoves[i] = EMPTY;
    }
}

void TicTacToe::print() {
    const string vertical = " | ", horizontal = "    --|---|--";

    cout << "    1   2   3" << endl << endl;
    cout << "1   " << displayValue(0) << vertical << displayValue(1) << vertical << displayValue(2) << "   3" << endl;
    cout << horizontal << endl;
    cout << "4   " << displayValue(3) << vertical << displayValue(4) << vertical << displayValue(5) << "   6" << endl;
    cout << horizontal << endl;
    cout << "7   " << displayValue(6) << vertical << displayValue(7) << vertical << displayValue(8) << "   9" << endl;
    cout << endl << "    7   8   9" << endl;
}

bool TicTacToe::userMove(int position) {
    if (this->userMoves[position] == EMPTY && this->pcMoves[position] == EMPTY) {
        this->userMoves[position] = X;
        this->pcMove();
        return true;
    }

    return false;
}

void TicTacToe::userMove() {
    cout << endl << endl << "Type your position. (1..9)" << endl << endl;

    int position;
    cin >> position;

    position--;

    if (!this->userMove(position)) {

        cout << "You are typed a invalid position. Please, try again.";
        this->userMove();
    }
}


string TicTacToe::displayValue(int position) {
    if (this->userMoves[position] != EMPTY) {
        return "X";
    } else if (this->pcMoves[position] != EMPTY) {
        return "O";
    } else {
        return "@";
    }
}

bool TicTacToe::pcMove(int position) {
    if (this->pcMoves[position] == EMPTY && this->userMoves[position] == EMPTY) {
        this->pcMoves[position] = O;
        return true;
    }

    return false;
}

void TicTacToe::pcMove() {
    if (defend(0, 4, 8)) {
        // First diagonal
    } else if (defend(2, 4, 6)) {
        // Second diagonal
    } else if (defend(0, 3, 6)) {
        // First column
    } else if (defend(1, 4, 7)) {
        // Second column
    } else if (defend(2, 5, 8)) {
        // Third column
    } else if (defend(0, 1, 2)) {
        // First row
    } else if (defend(3, 4, 5)) {
        // Second row
    } else if (defend(6, 7, 8)) {
        // Third row
    } else {
        srand(time(NULL));

        if (!pcMove(rand() % 9)) {
            pcMove();
        }
    }
}

bool TicTacToe::defend(int a, int b, int c) {
    if (isPlayedByUser(a) && isPlayedByUser(b) && !isPlayedByBoth(c)) {
        pcMove(c);
        return true;
    } else if (isPlayedByUser(a) && isPlayedByUser(c) && !isPlayedByBoth(b)) {
        pcMove(b);
        return true;
    } else if (isPlayedByUser(b) && isPlayedByUser(c) && !isPlayedByBoth(a)) {
        pcMove(a);
        return true;
    } else {
        return false;
    }
}

bool TicTacToe::wasTied() {
    int pc = 0, user = 0;

    for (register int i = 0; i < 9; i++) {
        if (this->pcMoves[i] != EMPTY) {
            pc++;
        }

        if (this->userMoves[i] != EMPTY) {
            user++;
        }
    }

    return pc >= 4 && user >= 4;
}

bool TicTacToe::isPlayedByUser(int position) {
    return this->userMoves[position] != EMPTY;
}

bool TicTacToe::isPlayedByPC(int position) {
    return this->pcMoves[position] != EMPTY;
}

bool TicTacToe::isPlayedByBoth(int position) {
    return isPlayedByUser(position) || isPlayedByPC(position);
}

bool TicTacToe::userWon() {
    return won(this->userMoves);
}

bool TicTacToe::pcWon() {
    return won(this->pcMoves);
}

bool TicTacToe::won(int moves[9]) {
    const bool firstDiagonal = moves[0] != EMPTY && moves[4] != EMPTY && moves[8] != EMPTY;
    const bool secondDiagonal = moves[2] != EMPTY && moves[4] != EMPTY && moves[6] != EMPTY;

    const bool firstColumn = moves[0] != EMPTY && moves[3] != EMPTY && moves[6] != EMPTY;
    const bool secondColumn = moves[1] != EMPTY && moves[4] != EMPTY && moves[7] != EMPTY;
    const bool thirdColumn = moves[2] != EMPTY && moves[5] != EMPTY && moves[8] != EMPTY;

    const bool firstRow = moves[0] != EMPTY && moves[1] != EMPTY && moves[2] != EMPTY;
    const bool secondRow = moves[3] != EMPTY && moves[4] != EMPTY && moves[5] != EMPTY;
    const bool thirdRow = moves[6] != EMPTY && moves[7] != EMPTY && moves[8] != EMPTY;

    return firstDiagonal || secondDiagonal
           || firstColumn || secondColumn || thirdColumn
           || firstRow || secondRow || thirdRow;
}